<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view("halaman.register");
    }

    public function kirim(Request $request){
        $nama_awal = $request['nama_awal'];
        $nama_akhir = $request['nama_akhir'];
        $alamat = $request['address'];


        return view('halaman.welcome', compact('nama_awal','nama_akhir','alamat'));
    }
}
