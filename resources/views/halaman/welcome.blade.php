@extends('layout.master')
    
    
@section('judul')
    <h1>Halaman Home</h1>
@endsection

@section('judul_card')
    Halaman Home
@endsection

@section('content')
    <h1>Selamat Datang! {{$nama_awal." ".$nama_akhir}}</h1>
    <p>Terima kasih telah bergabung di Website Kami. Media belajar kita bersama!</p>
    
@endsection
