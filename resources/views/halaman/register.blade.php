@extends('layout.master')

@section('judul')
    <h1>Halaman Form</h1>
@endsection

@section('judul_card')
    Halaman Form
@endsection

@section('content')
    <form action="/welcome" method="post">
        @csrf
        <label>First Name</label><br>
        <input type="text" name="nama_awal"><br><br>

        <label for="">Last Name</label><br>
        <input type="text" name="nama_akhir"><br><br>

        <label for="">Gender</label><br>
        <input type="radio" name="gender" value="male"> Male<br>
        <input type="radio" name="gender" value="female"> Female<br><br>

        <label>Nationality</label><br>
	    <select>
		    <option value="indonesia">Indonesia</option>
		    <option value="malaysia">Malaysia</option>
	    </select><br><br>

        <label>Language Spoken</label><br><br>
	    <input type="checkbox" name="bahasa" value="bind">Bahasa Indonesia<br>
	    <input type="checkbox" name="bahasa" value="bing">English<br>
	    <input type="checkbox" name="bahasa" value="other">Other<br><br><br>

        <label>Alamat</label><br>
        <textarea name="address" id="" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Kirim">

    </form>
@endsection   
        
